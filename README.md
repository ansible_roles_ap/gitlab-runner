GitLab runner
=========
Installs and configures gitlab runner.    

Include role
------------
```yaml
- name: gitlab_runner  
  src: https://gitlab.com/ansible_roles_v/gitlab-runner/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - gitlab_runner
```